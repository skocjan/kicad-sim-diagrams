# Kicad Simulation UML Diagrams

UML diagrams for Simulation in Kicad 7.0

Copyright by Sylwester Kocjan 2021. All rights reserved. 

## Getting started
In order to view these diagrams you will need to install BOUML https://www.bouml.fr/index.html
Project was prepared with version 7.11. For initial configuration of BOUML please consult documentation and chapter Authors

## Description
There is a major rework of Simulation Frame planned after KiCad 6.0 release. This project aims to present some ideas, which can be taken into consideration, discussed, improved or utilised.

## Authors
| Contributor      | User ID |
| ---------------- | ------- |
| Sylwester Kocjan | 2       |
